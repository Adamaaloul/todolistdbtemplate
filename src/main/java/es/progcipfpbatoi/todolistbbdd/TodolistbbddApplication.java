package es.progcipfpbatoi.todolistbbdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodolistbbddApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodolistbbddApplication.class, args);
	}

}
