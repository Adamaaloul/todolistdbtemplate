package es.progcipfpbatoi.todolistbbdd.modelo.repositorios;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.progcipfpbatoi.todolistbbdd.excepciones.AlreadyExistsException;
import es.progcipfpbatoi.todolistbbdd.excepciones.NotFoundException;
import es.progcipfpbatoi.todolistbbdd.modelo.dao.InMemoryTareaDAO;
import es.progcipfpbatoi.todolistbbdd.modelo.dao.SQLTareaDAO;
import es.progcipfpbatoi.todolistbbdd.modelo.dao.interfaces.TareaDao;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;

@Service
public class TareaRepository {

    private TareaDao tareaDAO;

   
    public TareaRepository(@Autowired SQLTareaDAO tareaDAO) {  //SQLTareaDAO //InMemoryTareaDAO
        this.tareaDAO = tareaDAO;
    }
    
    /**
     *  Devuelve el listado de todas las tareas.
     */
    public ArrayList<Tarea> findAll() {
        return tareaDAO.findAll();
    }

    /**
     *  Devuelve el listado de todas las tareas cuyo atributo nombre coincide con @user
     */
    public ArrayList<Tarea> findAll(String user) {
        return tareaDAO.findAllWithParams(user);
    }

    /**
     * Obtiene la Tarea con codigo @codTarea. En caso de que no la encuentre devolverá una excepción @NotFoundException
     * @param codigo
     */
    public Tarea get(int codigo) throws NotFoundException {
        return tareaDAO.getById(codigo);
    }
    
    /**
     * Obtiene la Tarea con codigo @codTarea. En caso de que no la encuentre devolverá null
     * @param codigo
     */
    public Tarea find(int codigo) {
        return tareaDAO.findById(codigo);
    }

    /**
     * Añade la tarea que contenga el código de la recibida como argumento si ésta no existe, o la actualiza en caso contrario
     * @param tarea
     */
    public void save(Tarea tarea) throws AlreadyExistsException, NotFoundException{
    	
    	if (find(tarea.getCodigo()) != null) {
    		tareaDAO.add(tarea);
    		return;
    	}
    	
        tareaDAO.update(tarea);
    }
    
    /**
     * Borra una tarea a partir de su código
     * @param tarea
     */
    public void remove(Tarea tarea) throws NotFoundException{
        tareaDAO.delete(tarea);
    }
}
