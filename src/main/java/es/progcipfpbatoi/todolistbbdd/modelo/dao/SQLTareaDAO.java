package es.progcipfpbatoi.todolistbbdd.modelo.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.progcipfpbatoi.todolistbbdd.excepciones.DatabaseErrorException;
import es.progcipfpbatoi.todolistbbdd.excepciones.NotFoundException;
import es.progcipfpbatoi.todolistbbdd.modelo.dao.interfaces.TareaDao;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Prioridad;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;
import es.progcipfpbatoi.todolistbbdd.modelo.services.MySQLConnection;

import java.security.Timestamp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

@Service
public class SQLTareaDAO implements TareaDao {

    private static final String DATABASE_TABLE = "tareas";

    @Autowired
    private MySQLConnection mySQLConnection;

    @Override
    public ArrayList<Tarea> findAll() {
    	ArrayList<Tarea> tareas = new ArrayList<>();
    
    	String sql = String.format("SELECT * FROM %s",DATABASE_TABLE);
    	
    	Connection connection = mySQLConnection.getConnection();
    	
    	try(PreparedStatement ps =connection.prepareStatement(sql))
    	{
    		ResultSet rs = ps.executeQuery();
    		
    		while (rs.next()) {
				int codigo = rs.getInt("codigo");
				String usuario = rs.getString("usuario");
				String descripcion = rs.getString("descripcion");
				LocalDateTime fechaCreacion = rs.getTimestamp("fechaCreacion").toLocalDateTime();
				Prioridad prioridad = Prioridad.fromText(rs.getString("prioridad"));
				LocalDateTime vencimiento = rs.getTimestamp("vencimiento").toLocalDateTime();
				boolean realizada = rs.getInt("realizada")==1;
				
				Tarea tarea = new Tarea(codigo, usuario, descripcion, fechaCreacion, prioridad , vencimiento.toLocalDate(),  
						vencimiento.toLocalTime(), realizada);
				tareas.add(tarea);
			}
    		return tareas;
    		
    	}catch (SQLException e) {
            throw new DatabaseErrorException(sql + e.getMessage());
        }
		
    }

    @Override
    public ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate vencimiento, String usuario) {
    	

        throw new RuntimeException("Not yet implemented");
    	
    }
    	
    @Override
    public ArrayList<Tarea> findAllWithParams(String usuario) {
        throw new RuntimeException("Not yet implemented");
    }
    
    @Override
    public Tarea getById(int id) throws NotFoundException {
    	String sql = String.format("SELECT * FROM %s WHERE codigo = ? ", DATABASE_TABLE);

    	Connection connection = mySQLConnection.getConnection();
    	
        try (PreparedStatement ps = connection.prepareStatement(sql))
        {
        	ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return mapToTarea(rs);
            }
            
            throw new NotFoundException("La tarea no existe");
            
        } catch (SQLException e) {
            throw new DatabaseErrorException(sql + e.getMessage());
        }
    }

    
    @Override
    public Tarea findById(int id) {
        throw new RuntimeException("Not yet implemented");
    }

    
    @Override
    public void add(Tarea tarea) {
        throw new RuntimeException("Not yet implemented");
    }
    
    
    @Override
    public void update(Tarea tarea) throws NotFoundException {
    	throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void delete(Tarea tarea) throws NotFoundException{
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Extrae de @resultset todos los datos asociados a una tarea para crearla y devolverla
     *
     * @param resultset
     * @return Tarea
     */
    private Tarea mapToTarea(ResultSet resultset) throws SQLException {
    	int cod = resultset.getInt("codigo");
		String usuario = resultset.getString("usuario");
		String descripcion = resultset.getString("descripcion");
		LocalDateTime creadoEn = resultset.getTimestamp("fechaCreacion").toLocalDateTime();
		Prioridad priority = Prioridad.fromText(resultset.getString("prioridad"));
		LocalDateTime vencimiento = resultset.getTimestamp("vencimiento").toLocalDateTime();
		boolean realizada = resultset.getBoolean("realizada");
		
		return new Tarea(cod, usuario, descripcion, creadoEn, priority, vencimiento.toLocalDate(), 
				vencimiento.toLocalTime(), realizada);
    }

    
}
