package es.progcipfpbatoi.todolistbbdd.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.progcipfpbatoi.todolistbbdd.excepciones.AlreadyExistsException;
import es.progcipfpbatoi.todolistbbdd.excepciones.NotFoundException;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Prioridad;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;
import es.progcipfpbatoi.todolistbbdd.modelo.repositorios.TareaRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TareaController {

    @Autowired
    private TareaRepository tareaRepository;
    
    @GetMapping("/tarea-add")
    public String tareaFormActionView(){
        return "tarea_form_view";
    }

    @PostMapping(value = "/tarea-add")
    public String postAddAction(@RequestParam Map<String, String> params, RedirectAttributes redirectAttributes) {
    	try {
	    	int code = Integer.parseInt(params.get("code"));
	        String user = params.get("user");
	        String descripcion = params.get("description");
	        String prioridad = params.get("priority");
	        String realizadaString = params.get("realizada");
	        boolean realizada = realizadaString != null && realizadaString.equalsIgnoreCase("true");
	        Prioridad priority = Prioridad.fromText(prioridad);
	        String horaExpiracionString = params.get("expiration_time");
	        String fechaExpiracionString = params.get("expiration_date");
	        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        LocalDate fecha = LocalDate.parse(fechaExpiracionString, dateFormatter);
	        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
	        LocalTime hora = LocalTime.parse(horaExpiracionString, timeFormatter);
	        Tarea tarea = new Tarea(code, user, descripcion, priority, fecha, hora, realizada);
	        tareaRepository.save(tarea);
	        redirectAttributes.addFlashAttribute("infoMessage", "Tarea insertada con éxito");
        } catch (NotFoundException | AlreadyExistsException ex) {
        	HashMap<String, String> errors = new HashMap<>();
        	errors.put("código", ex.getMessage());
        	redirectAttributes.addFlashAttribute("errors", errors);
        }
    	
    	return "redirect:/tareas";
        
    }
    
    @GetMapping(value = "/tarea-del")
    @ResponseBody
    public String deleteAction(@RequestParam int codTarea) throws NotFoundException {
        Tarea tarea = tareaRepository.get(codTarea);
        tareaRepository.remove(tarea);
        return "Tarea borrada " + codTarea + "con éxito";
    }

    @GetMapping("/tarea")
    public String getTareaView(@RequestParam int codTarea, Model model) throws NotFoundException {
        Tarea tarea = tareaRepository.get(codTarea);
        model.addAttribute("tarea", tarea);
        model.addAttribute("usuarioLogueado", "Roberto");
        return "tarea_details_view";
    }

    @GetMapping(value = "/tareas")
    public String tareaList(Model model, @RequestParam HashMap<String, String> params) {
        String usuario = params.get("usuario");
        ArrayList<Tarea> tareas;
        if (usuario != null && !usuario.isEmpty()) {
            tareas = tareaRepository.findAll(usuario);
            model.addAttribute("usuario", usuario);
        } else {
            tareas = tareaRepository.findAll();
        }
        model.addAttribute("tareas", tareas);
        return "list_tarea_view";
    }

}
